import { Negociacao } from "../models/negociacao.js";
import { Negociacoes } from "../models/negociacoes.js";

export class Negociacaocontroller {
    private inputData: HTMLInputElement;  
    private inputQuantidade: HTMLInputElement; 
    private inputValor: HTMLInputElement;
    private negociacoes: Negociacoes = new Negociacoes();

    constructor() { 
        this.inputData = document.querySelector("#data");
        this.inputQuantidade = document.querySelector("#quantidade");
        this.inputValor = document.querySelector("#valor");
    }

    adiciona() { 
        const negociao = this.criaNegociacao();
        negociao.adicionar();
        this.negociacoes.adiciona(negociao);
        console.log('Negociacao incluída com sucesso.');
        console.log(this.negociacoes.lista());
        this.limparFormulario();
    }  
    
    private criaNegociacao(): Negociacao{
        const exp = /-/g;
        const date = new Date(this.inputData.value.replace(exp, ','));
        const quantidade = parseInt(this.inputQuantidade.value);
        const valor = parseFloat(this.inputValor.value);

        return new Negociacao(date, quantidade, valor);
    }
    private limparFormulario(): void {
        this.inputData.value = '';
        this.inputQuantidade.value = '';
        this.inputValor.value = '';
        
        this.inputData.focus();
    }

}