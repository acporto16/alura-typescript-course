export class Negociacao {
    private _data: Date; 
    private _quantidade: number; 
    private _valor: number;

    constructor(data: Date, quantidade: number, valor: number) { 
        this._data = data; 
        this._quantidade = quantidade; 
        this._valor = valor; 
    }   
    
    data(){
        return this._data;
    }

    quantidade(){
        return this._quantidade;
    }

    valor(){
        return this._valor;
    }

    adicionar(){
        console.log("teste inclusão negociacao...");
        console.log(this._data);        
        console.log("Valor Total da Compra: " + this._valor * this._quantidade);
    }    
}